<?php

use App\Http\Controllers\DepartementController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\SuratController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('auth.login-custom');
})->name('login.custom');

Route::get('/admin', [UserController::class, 'dashboardAdmin'])->name('dashboard.admin')->middleware('is_admin');

Route::group(['prefix' => 'department'], function () {
    Route::get('', [DepartementController::class, 'index'])->name('dept.index');
    Route::get('/create', [DepartementController::class, 'createDept'])->name('dept.create.view');
    Route::get('/update/{id}', [DepartementController::class, 'updateDept'])->name('dept.update.view');
    Route::post('/store', [DepartementController::class, 'storeDept'])->name('dept.store');
    Route::post('/edit/{id}', [DepartementController::class, 'editDept'])->name('dept.edit');
    Route::get('/delete/{id}', [DepartementController::class, 'deleteDept'])->name('dept.delete');
});
Route::group(['prefix' => 'kategori'], function () {
    Route::get('', [KategoriController::class, 'index'])->name('kategori.index');
    Route::get('/create', [KategoriController::class, 'createKategori'])->name('kategori.create.view');
    Route::get('/update/{id}', [KategoriController::class, 'updateKategori'])->name('kategori.update.view');
    Route::post('/store', [KategoriController::class, 'storeKategori'])->name('kategori.store');
    Route::post('/edit/{id}', [KategoriController::class, 'editKategori'])->name('kategori.edit');
    Route::get('/delete/{id}', [KategoriController::class, 'deleteKategori'])->name('kategori.delete');
    Route::get('/kategori-by-jenis', [KategoriController::class, 'kategoriByJenis'])->name('kategori.by.jenis');
});
Route::group(['prefix' => 'surat'], function () {
    Route::get('', [SuratController::class, 'index'])->name('surat.index');
    Route::get('/create-masuk', [SuratController::class, 'createSuratMasuk'])->name('surat.create.view.masuk');
    Route::get('/create-keluar', [SuratController::class, 'createSuratKeluar'])->name('surat.create.view.keluar');
    Route::get('/update/{id}', [SuratController::class, 'updateSurat'])->name('surat.update.view');
    Route::post('/store', [SuratController::class, 'storeSurat'])->name('surat.store');
    Route::post('/edit/{id}', [SuratController::class, 'editSurat'])->name('surat.edit');
    Route::get('/delete/{id}', [SuratController::class, 'deleteSurat'])->name('surat.delete');
    Route::get('/detail/{id}', [SuratController::class, 'detailSurat'])->name('surat.detail');
    Route::get('/filter', [SuratController::class, 'filterDaftarSuratAdmin'])->name('filter.surat.admin');
});

Route::group(['prefix' => 'laporan'], function () {
    Route::get('/admin', [SuratController::class, 'laporanAdmin'])->name('laporan.surat.admin');
    Route::get('/admin/filter', [SuratController::class, 'filterLaporanSuratAdmin'])->name('filter.laporan.admin');
});

Route::group(['prefix' => 'user'], function () {
    // page admin
    Route::get('', [UserController::class, 'index'])->name('user.index');
    Route::get('/create', [UserController::class, 'createUser'])->name('user.create.view');
    Route::get('/update/{id}', [UserController::class, 'updateUser'])->name('user.update.view');
    Route::post('/store', [UserController::class, 'storeUser'])->name('user.store');
    Route::post('/edit/{id}', [UserController::class, 'editUser'])->name('user.edit');
    Route::get('/delete/{id}', [UserController::class, 'deleteUser'])->name('user.delete');

    // page user
    Route::get('/surat/{id_user}', [SuratController::class, 'indexUser'])->name('user.surat.index');
    Route::get('/surat/detail/{id}', [SuratController::class, 'detailSuratUser'])->name('user.surat.detail');
    Route::get('/surat/detail/laporan/{id}', [SuratController::class, 'detailSuratUserLaporan'])->name('user.surat.detail.laporan');
    Route::get('/laporan/{id}', [SuratController::class, 'laporanUser'])->name('user.laporan');
    Route::get('/surat/filter/{id}', [SuratController::class, 'filterDaftarSuratUser'])->name('filter.surat.user');
    Route::get('/laporan/filter/{id}', [SuratController::class, 'filterLaporanSuratUser'])->name('filter.laporan.user');
    Route::post('/surat/konfirmasi/{id}', [SuratController::class, 'konfirmasiSurat'])->name('surat.confirm');

    Route::get('/dashboard/{id_user}', [UserController::class, 'dashboardUser'])->name('dashboard.user');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
