@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Input User</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8">
                            <form role="form" class="text-start" action="{{ route('user.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <strong>Username : </strong><br>
                                <div class="input-group input-group-outline my-2">
                                    <input type="text" class="form-control" name="username" placeholder="Input Username">
                                </div>
                                <strong>Email : </strong><br>
                                <div class="input-group input-group-outline my-2">
                                    <input type="email" class="form-control" name="email" placeholder="Input Email">
                                </div>
                                <strong>password : </strong><br>
                                <div class="input-group input-group-outline my-2">
                                    <input type="text" class="form-control" name="password" placeholder="Input Password">
                                </div>
                                <strong>Department : </strong><br>
                                <div class="input-group input-group-outline">
                                    <select class="input-group input-group-outline my-2 form-control" name="id_department">
                                        <option selected disabled>Pilih Departement</option>
                                        @foreach ($dept as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-6"></div>
                                    <div class="col-3">
                                        <a href="{{ route('user.index') }}" class="btn bg-gradient-primary w-100 my-4 mb-2">Cancel</a>
                                    </div>
                                    <div class="col-3">
                                        <button type="submit" class="btn bg-gradient-primary w-100 my-4 mb-2">Sumbit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
