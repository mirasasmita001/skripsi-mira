@extends('layouts.admin')

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger">
    <p>{{ $message }}</p>
</div>
@endif
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Daftar Kategori</h6>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <a href="{{ route('kategori.create.view') }}"
                                class="btn bg-gradient-primary w-50 my-4 mb-2">Tambah
                                Kategori</a>
                        </div>
                        <div class="col-2"></div>
                        <div class="col-4 my-auto">
                            <form action="{{ route('kategori.by.jenis') }}" method="GET">
                                <div class="row">
                                    <div class="col-6 my-4">
                                        <div class="input-group input-group-outline">
                                            <select
                                                class="input-group input-group-outline my-2 form-select form-control form-select mb-2"
                                                name="jenis">
                                                <option selected disabled>Pilih Jenis Surat</option>
                                                <option value="masuk">Masuk</option>
                                                <option value="keluar">Keluar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 my-4">
                                        <button type="submit"
                                            class="btn bg-gradient-primary w-100 my-2 mb-2">filter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="table-responsive p-0">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                    No</th>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                    Kode</th>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                    Nama</th>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                    Jenis</th>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2 text-center">
                                                    Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($kategori as $index => $data)
                                            <tr>
                                                <td>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $index + 1 }}</h6>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $data->kode }}</h6>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $data->nama }}</h6>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $data->jenis_surat }}</h6>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <a class=" btn badge badge-sm bg-gradient-success"
                                                        href="{{ route('kategori.update.view', $data->id) }}">
                                                        <i class="material-icons opacity-10">edit</i>
                                                    </a>
                                                    <a class="btn badge badge-sm bg-gradient-danger"
                                                        href="{{ route('kategori.delete', $data->id) }}">
                                                        <i class="material-icons opacity-10">delete</i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td class="text-center" colspan="5">
                                                    <p>Daftar Kategori Tidak Ditemukan</p>
                                                </td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection