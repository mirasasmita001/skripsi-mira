@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Edit Surat</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8">
                            <form role="form" class="text-start" action="{{ route('surat.edit', $data->id) }}"
                                method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-6">
                                        <strong>Nama : </strong><br>
                                        <div class="input-group input-group-outline my-2">
                                            <input type="text" class="form-control" name="nama"
                                                placeholder="Edit Nama Surat" value="{{ $data->nama }}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <strong>Tanggal : </strong><br>
                                        <div class="input-group input-group-outline my-2">
                                            <input type="date" class="form-control" name="tanggal"
                                                placeholder="Edit Tanggal Surat" value="{{ $data->tanggal }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <strong>Kategori : </strong><br>
                                        <div class="input-group input-group-outline">
                                            <select class="input-group input-group-outline my-2 form-control"
                                                name="id_kategori">
                                                <option disabled>Pilih Kategori</option>
                                                @foreach ($kategori as $ktg)
                                                <option value="{{ $ktg->id }}"
                                                    {{ $ktg->id == $data->id_kategori ? 'selected' : '' }}>
                                                    {{ $ktg->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <strong>Tujuan : </strong><br>
                                        <div class="input-group input-group-outline">
                                            <select class="input-group input-group-outline my-2 form-control"
                                                name="id_user">
                                                <option selected disabled>Pilih Tujuan</option>
                                                @foreach ($user as $usr)
                                                <option value="{{ $usr->id }}"
                                                    {{ $usr->id == $data->id_user ? 'selected' : '' }}>
                                                    {{ $usr->username }} -
                                                    {{ $usr->dept->nama }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <strong>file : </strong><br>
                                <div class="input-group input-group-outline my-2">
                                    <input type="file" class="form-control" name="file"
                                        placeholder="Pilih file jika ingin update !"
                                        value="{{ Storage::url('$data->file') }}">
                                </div>
                                <div class="row">
                                    <div class="col-6"></div>
                                    <div class="col-3">
                                        <a href="{{ route('surat.index') }}"
                                            class="btn bg-gradient-primary w-100 my-4 mb-2">Cancel</a>
                                    </div>
                                    <div class="col-3">
                                        <button type="submit"
                                            class="btn bg-gradient-primary w-100 my-4 mb-2">Sumbit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection