@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Detail Surat</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-6 my-1">
                                    <table>
                                        <tr>
                                            <td>Nomor Surat</td>
                                            <td>:</td>
                                            <td><strong>{{ $data->nomor }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Surat</td>
                                            <td>:</td>
                                            <td><strong>{{ $data->nama }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Tujuan Surat</td>
                                            <td>:</td>
                                            <td><strong>{{ $data->user->username }} -
                                                    {{ $data->user->dept->nama }}</strong></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-6 my-1">
                                    <table>
                                        <tr>
                                            <td>Kategori Surat</td>
                                            <td>:</td>
                                            <td><strong>{{ $data->kategori->nama }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Surat</td>
                                            <td>:</td>
                                            <td><strong>{{ $data->kategori->jenis_surat }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>
                                                @if ($data->status == 1)
                                                <strong>Belum Dibaca</strong>
                                                @elseif ($data->status == 2)
                                                <strong>Sudah Dibaca</strong>
                                                @else
                                                <strong>Terkonfirmasi</strong>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            Catatan :
                            <div class="row">
                                @if ($data->catatan == null)
                                <div class="col-12 my-1"><strong>Tidak Ada Catatan</strong></div>
                                @else
                                <div class="col-11 my-1">
                                    <p class="text-justify">{{$data->catatan}}</p>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-6 my-1"> File Surat :</div>
                                <div class="col-6 my-1">Tanggal : <strong>{{ $data->tanggal }}</strong></div>
                            </div>
                            <div class="row">
                                <!-- <div class="col-12 my-1">
                                    <iframe src="{{ Storage::url($data->file) }}" width="600" height="700"
                                        visible="true" style="OVERFLOW: hidden"></iframe>
                                </div> -->
                                <div class="col-12 my-1">
                                    <!-- <embed src='{{ Storage::url($data->file) }}' width='100%' height='100%'
                                        type="application/pdf" /> -->
                                    <iframe style="OVERFLOW: hidden" height="1000" src="{{ Storage::url($data->file) }}"
                                        frameborder="0" width="800" marginwidth="0" scrolling="no"></iframe>
                                    <!-- <iframe src="" width="600" height="700" visible="true"></iframe> -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9"></div>
                                <div class="col-3">
                                    <a href="{{ route('surat.index') }}"
                                        class="btn bg-gradient-primary w-100 my-4 mb-2">Back</a>\
                                </div>
                            </div>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection