@extends('layouts.user')

@section('content')

<div class="row mt-4">
    <div class="col-lg-2 col-md-6 mt-4 mb-4">
    </div>
    <div class="col-lg-4 col-md-6 mt-4 mb-4">
        <div class="card z-index-2 ">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 bg-transparent">
            <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                <center><i class="material-icons opacity-10" style="font-size:200px;color:white;">receipt_long</i></center>
            </div>
        </div>
        </div>
        <div class="card-body">
            <h6 class="mb-0 ">Surat Hari Ini</h6>
            <p class="text-sm "> (<span class="font-weight-bolder">{{ $suratToday }}</span>) Total Surat Hari . </p>
            <hr class="dark horizontal">
            <div class="d-flex ">
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 mt-4 mb-4">
        <div class="card z-index-2 ">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 bg-transparent">
            <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                <center><i class="material-icons opacity-10" style="font-size:200px;color:white;">receipt_long</i></center>
            </div>
        </div>
        </div>
        <div class="card-body">
            <h6 class="mb-0 ">Belum dibaca</h6>
            <p class="text-sm "> (<span class="font-weight-bolder">{{ $noRead }}</span>) Total Surat Belum dibaca. </p>
            <hr class="dark horizontal">
            <div class="d-flex ">
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-6 mt-4 mb-4">
    </div>
</div>

<div class="row mt-4">
    <div class="col-lg-2 col-md-6 mt-4 mb-4">
    </div>
    <div class="col-lg-4 col-md-6 mt-4 mb-4">
        <div class="card z-index-2 ">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 bg-transparent">
            <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                <center><i class="material-icons opacity-10" style="font-size:200px;color:white;">receipt_long</i></center>
            </div>
        </div>
        </div>
        <div class="card-body">
            <h6 class="mb-0 ">Laporan Surat</h6>
            <p class="text-sm "> (<span class="font-weight-bolder">{{ $laporan }}</span>) Total Laporan Surat. </p>
            <hr class="dark horizontal">
            <div class="d-flex ">
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 mt-4 mb-4">
        <div class="card z-index-2 ">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 bg-transparent">
            <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                <center><i class="material-icons opacity-10" style="font-size:200px;color:white;">receipt_long</i></center>
            </div>
        </div>
        </div>
        <div class="card-body">
            <h6 class="mb-0 ">Total Surat</h6>
            <p class="text-sm "> (<span class="font-weight-bolder">{{ $totalSurat }}}</span>) Total Surat Masuk dan Surat Keluar. </p>
            <hr class="dark horizontal">
            <div class="d-flex ">
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-6 mt-4 mb-4">
    </div>
</div>

@endsection
