@extends('layouts.user')

@section('content')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Daftar Surat</h6>
                        </div>
                        <div class="card-body px-0 pb-2">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center mb-0">
                                    <thead>
                                        <tr>
                                            <!-- <div class="col-4"></div> -->
                                            <div class="row">
                                                <div class="col-12">
                                                    <form
                                                        action="{{ route('filter.laporan.user', auth()->user()->id) }}"
                                                        method="GET">
                                                        <div class="row">
                                                            <div class="col-2 my-3">
                                                                <div class="input-group input-group-outline">
                                                                    <select
                                                                        class="input-group input-group-outline my-2 form-select form-control form-select mb-2"
                                                                        name="kategori">
                                                                        <option selected disabled>Kategori</option>
                                                                        @foreach ($kategori as $data)
                                                                        <option value="{{ $data->nama }}">
                                                                            {{ $data->nama }}
                                                                        </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-2 my-3">
                                                                <div class="input-group input-group-outline">
                                                                    <select
                                                                        class="input-group input-group-outline my-2 form-select form-control form-select mb-2"
                                                                        name="jenis">
                                                                        <option selected disabled>Jenis</option>
                                                                        <option value="masuk">Masuk</option>
                                                                        <option value="keluar">Keluar</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-2 my-4">
                                                                <div class="input-group input-group-outline">
                                                                    <input type="date" class="form-control"
                                                                        name="tanggal" placeholder="Tanggal">
                                                                </div>
                                                            </div>
                                                            <div class="col-2 my-3">
                                                                <button type="submit"
                                                                    class="btn bg-gradient-primary w-100 my-2 mb-2">filter</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                            </div>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                No</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Nomor Surat</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Nama</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Kategori</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Jenis</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                Tanggal</th>
                            <th
                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2 text-center">
                                Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse ($surat as $index => $data)
                                <tr>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $index + 1 }}</h6>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $data->nomor }}</h6>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $data->nama }}</h6>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $data->kategori->nama }}</h6>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $data->kategori->nama }}</h6>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $data->tanggal }}</h6>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <a class=" btn badge badge-sm bg-gradient-success"
                                            href="{{ route('user.surat.detail.laporan', $data->id) }}">
                                            <i class="material-icons opacity-10">remove_red_eye</i>
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="8">
                                        <p>Daftar Surat Tidak Ditemukan</p>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection