<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Surat extends Model
{
    use HasFactory, SoftDeletes, Notifiable;

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'id_kategori', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id')->with('dept');
    }
}
