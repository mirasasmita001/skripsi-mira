<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Surat;
use App\Models\User;
use App\Notifications\SuratEmailNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SuratController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $kategori = Kategori::all();
        $user = User::where('is_admin', false)->get();
        $surat = Surat::with(['kategori', 'user'])
            ->where('status', '!=', 3)
            ->get();
        return view('surat_admin.daftarsurat-admin', compact('surat', 'kategori', 'user'));
    }

    public function indexUser($id_user)
    {
        $kategori = Kategori::all();
        $surat = Surat::with(['kategori', 'user'])
            ->where('id_user', $id_user)
            ->where('status', '!=', 3)
            ->get();
        return view('user.daftarsurat-user', compact('surat', 'kategori'));
    }

    public function createSuratMasuk()
    {
        $user = User::with('dept')
            ->where('status_active', true)
            ->where('is_admin', false)->get();
        $kategori = Kategori::where('status_active', true)->where('jenis_surat', 'masuk')->get();
        return view('surat_admin.input-surat-masuk', compact('user', 'kategori'));
    }

    public function createSuratKeluar()
    {
        $user = User::with('dept')
            ->where('status_active', true)
            ->where('is_admin', false)->get();
        $kategori = Kategori::where('status_active', true)->where('jenis_surat', 'keluar')->get();
        return view('surat_admin.input-surat-keluar', compact('user', 'kategori'));
    }

    public function storeSurat(Request $request)
    {

        $file = $request->file('file');
        $path = Storage::putFile('public/surat', $file);

        $surat = new Surat;
        if (isset($request->no_surat)) {
            $surat->nomor = $request->no_surat;
        } else {
            $year = Carbon::now()->format('Y');
            $lastId = Surat::whereHas('kategori', function ($q) {
                $q->where('jenis_surat', 'keluar');
            })->latest()->first();

            if (empty($lastId->id)) {
                $lastId = 0;
            } else {
                $lastId = $lastId->id;
            }

            $surat->nomor           = str_pad($lastId + 1, 3, 0, STR_PAD_LEFT) . '/1301/LBN/' . $year;
        }
        $surat->nama            = $request->nama;
        $surat->id_kategori     = $request->id_kategori;
        $surat->id_user         = $request->id_user;
        $surat->tanggal         = $request->tanggal;
        $surat->file            = $path;
        $surat->save();
        $user = User::find($surat->id_user);
        $user->notify(new SuratEmailNotification($surat, $user));
        return redirect()->route('surat.index')->with('success', 'Data surat berhasil ditambahkan');
    }

    public function updateSurat($id)
    {
        $data = Surat::find($id);
        $user = User::with('dept')
            ->where('status_active', true)
            ->where('is_admin', false)->get();
        $kategori = Kategori::where('status_active', true)->get();
        return view('surat_admin.edit-surat', compact('data', 'user', 'kategori'));
    }

    public function editSurat(Request $request, $id)
    {
        $surat = Surat::find($id);
        if (isset($request->file)) {
            $file_name = $request->file('file');
            $path = Storage::putFile('public/surat', $file_name);
            $surat->file        = $path;
        }

        $surat->nama            = $request->nama;
        $surat->id_kategori     = $request->id_kategori;
        $surat->id_user         = $request->id_user;
        if (isset($request->tanggal)) {
            $surat->tanggal         = $request->tanggal;
        }
        $surat->save();
        return redirect()->route('surat.index')->with('success', 'Data surat berhasil diupdate');
    }

    public function deleteSurat($id)
    {
        $data = Surat::find($id);
        $data->delete();
        return redirect()->route('surat.index')->with('error', 'Data surat berhasil dihapus');
    }

    public function detailSuratAdmin($id)
    {
        $data = Surat::with(['kategori', 'user'])->find($id);
        return view('surat_admin.detail-surat', compact('data'));
    }

    public function konfirmasiSurat(Request $request, $id)
    {
        $data = Surat::find($id);
        $data->status = 3;
        if (isset($request->catatan)) {
            $data->catatan = $request->catatan;
        }
        $data->save();
        return redirect()->route('user.surat.index', auth()->user()->id)->with('success', 'Data surat berhasil di konfirmasi');
    }


    public function detailSurat($id)
    {
        $data = Surat::with(['kategori', 'user'])->find($id);

        return view('surat_admin.detail-surat', compact('data'));
    }

    public function detailSuratUser($id)
    {
        $data = Surat::with(['kategori', 'user'])->find($id);
        $data->status = 2;
        $data->save();
        return view('user.detail-surat', compact('data'));
    }

    public function detailSuratUserLaporan($id)
    {
        $data = Surat::with(['kategori', 'user'])->find($id);
        return view('user.detail-surat-laporan', compact('data'));
    }


    public function laporanAdmin(Request $request)
    {
        $kategori = Kategori::all();
        $user = User::where('is_admin', false)->get();
        $surat = Surat::with(['kategori', 'user'])
            ->where('status', 3)
            ->get();
        return view('surat_admin.laporan-admin', compact('surat', 'kategori', 'user'));
    }

    public function laporanUser($id)
    {
        $kategori = Kategori::all();
        $surat = Surat::with(['kategori', 'user'])
            ->where('id_user', $id)
            ->where('status', 3)->get();
        return view('user.laporan-user', compact('surat', 'kategori'));
    }

    public function filterDaftarSuratAdmin(Request $request)
    {

        $kategori = $request->kategori;
        $tujuan = $request->tujuan;
        $status = $request->status;
        $jenis = $request->jenis;
        $tanggal = $request->tanggal;
        $surats = Surat::with(['kategori', 'user'])->where('status', '!=', 3);

        if (isset($kategori)) {
            $surats->whereHas('kategori', function ($q) use ($kategori) {
                $q->where('nama', $kategori);
            })->get();
        }
        if (isset($tujuan)) {
            $surats->whereHas('user', function ($q) use ($tujuan) {
                $q->where('username', $tujuan);
            })->get();
        }
        if (isset($jenis)) {
            $surats->whereHas('kategori', function ($q) use ($jenis) {
                $q->where('jenis_surat', $jenis);
            })->get();
        }
        if (isset($status)) {
            $surats->where('status', $status)->get();
        }
        if (isset($tanggal)) {
            $surats->where('tanggal', $tanggal);
        }

        $surat = $surats->get();
        $kategori = Kategori::all();
        $user = User::where('is_admin', false)->get();

        return view('surat_admin.daftarsurat-admin', compact('surat', 'kategori', 'user'));
    }

    public function filterLaporanSuratAdmin(Request $request)
    {

        $kategori = $request->kategori;
        $tujuan = $request->tujuan;
        $status = $request->status;
        $jenis = $request->jenis;
        $tanggal = $request->tanggal;
        $surats = Surat::with(['kategori', 'user'])->where('status', 3);

        if (isset($kategori)) {
            $surats->whereHas('kategori', function ($q) use ($kategori) {
                $q->where('nama', $kategori);
            })->get();
        }
        if (isset($tujuan)) {
            $surats->whereHas('user', function ($q) use ($tujuan) {
                $q->where('username', $tujuan);
            })->get();
        }
        if (isset($jenis)) {
            $surats->whereHas('kategori', function ($q) use ($jenis) {
                $q->where('jenis_surat', $jenis);
            })->get();
        }
        if (isset($status)) {
            $surats->where('status', $status)->get();
        }
        if (isset($tanggal)) {
            $surats->where('tanggal', $tanggal);
        }

        $surat = $surats->get();
        $kategori = Kategori::all();
        $user = User::where('is_admin', false)->get();

        return view('surat_admin.laporan-admin', compact('surat', 'kategori', 'user'));
    }

    public function filterDaftarSuratUser(Request $request, $id_user)
    {

        $kategori = $request->kategori;
        $status = $request->status;
        $jenis = $request->jenis;
        $tanggal = $request->tanggal;
        $surats = Surat::with(['kategori', 'user'])
            ->where('id_user', $id_user)
            ->where('status', '!=', 3);

        if (isset($kategori)) {
            $surats->whereHas('kategori', function ($q) use ($kategori) {
                $q->where('nama', $kategori);
            })->get();
        }
        if (isset($tujuan)) {
            $surats->whereHas('user', function ($q) use ($tujuan) {
                $q->where('username', $tujuan);
            })->get();
        }
        if (isset($jenis)) {
            $surats->whereHas('kategori', function ($q) use ($jenis) {
                $q->where('jenis_surat', $jenis);
            })->get();
        }
        if (isset($status)) {
            $surats->where('status', $status)->get();
        }
        if (isset($tanggal)) {
            $surats->where('tanggal', $tanggal);
        }

        $surat = $surats->get();
        $kategori = Kategori::all();
        $user = User::where('is_admin', false)->get();

        return view('user.daftarsurat-user', compact('surat', 'kategori', 'user'));
    }

    public function filterLaporanSuratUser(Request $request, $id_user)
    {

        $kategori = $request->kategori;
        $jenis = $request->jenis;
        $tanggal = $request->tanggal;
        $surats = Surat::with(['kategori', 'user'])
            ->where('id_user', $id_user)
            ->where('status', 3);

        if (isset($kategori)) {
            $surats->whereHas('kategori', function ($q) use ($kategori) {
                $q->where('nama', $kategori);
            })->get();
        }

        if (isset($jenis)) {
            $surats->whereHas('kategori', function ($q) use ($jenis) {
                $q->where('jenis_surat', $jenis);
            })->get();
        }
        if (isset($tanggal)) {
            $surats->where('tanggal', $tanggal);
        }

        $surat = $surats->get();
        $kategori = Kategori::all();
        $user = User::where('is_admin', false)->get();

        return view('user.laporan-user', compact('surat', 'kategori', 'user'));
    }
}
