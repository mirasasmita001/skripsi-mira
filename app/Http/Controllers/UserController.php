<?php

namespace App\Http\Controllers;

use App\Models\Departement;
use App\Models\Kategori;
use App\Models\Surat;
use App\Models\User;
use App\Notifications\SuratEmailNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function dashboardUser($id_user)
    {
        $noRead = Surat::where('status', 1)->where('id_user', $id_user)->get()->count();
        $laporan = Surat::where('status', 3)->where('id_user', $id_user)->get()->count();
        $totalSurat = Surat::where('id_user', $id_user)->get()->count();
        $suratToday = Surat::where('id_user', $id_user)->whereDate('created_at', Carbon::today())->get()->count();
        return view('user.dashboard-user', compact('noRead', 'laporan', 'totalSurat', 'suratToday'));
    }

    public function dashboardAdmin()
    {
        $noRead = Surat::where('status', 1)->get()->count();
        $laporan = Surat::where('status', 3)->get()->count();
        $totalSurat = Surat::get()->count();
        return view('dashboard-admin', compact('noRead', 'laporan', 'totalSurat'));
    }

    public function index(Request $request)
    {
        $user = User::with('dept')
            ->where('status_active', true)
            ->where('is_admin', false)->get();
        return view('user_admin.daftar-user', compact('user'));
    }

    public function createUser()
    {
        $dept = Departement::where('status_active', true)->get();
        return view('user_admin.input-user', compact('dept'));
    }

    public function storeUser(Request $request)
    {
        $user = new User;
        $user->username =  $request->username;
        $user->email = $request->email;
        $pass = Hash::make($request->password);
        $user->password = $pass;
        $user->id_departement = $request->id_department;
        $user->save();
        return redirect()->route('user.index')->with('success', 'Data user berhasil ditambahkan');
    }

    public function updateUser($id)
    {
        $data = User::find($id);
        $dept = Departement::where('status_active', true)->get();
        return view('user_admin.edit-user', compact('data', 'dept'));
    }

    public function editUser(Request $request, $id)
    {
        $data = User::find($id);
        $data->username =  $request->username;
        $data->email = $request->email;
        $data->id_departement = $request->id_department;
        $data->save();
        return redirect()->route('user.index')->with('success', 'Data user berhasil diupdate');
    }

    public function deleteUser($id)
    {
        $data = User::find($id);
        $data->status_active = false;
        $data->save();
        return redirect()->route('user.index')->with('error', 'Data user berhasil dihapus');
    }
}
