<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{


    public function index(Request $request)
    {
        $kategori = Kategori::where('status_active', true)->get();
        return view('kategori_admin.daftar-kategori', compact('kategori'));
    }

    public function createKategori()
    {
        return view('kategori_admin.input-kategori');
    }

    public function storeKategori(Request $request)
    {
        $lastId = Kategori::where('status_active', true)->latest()->first();
        // dd($lastId);
        if (empty($lastId->id)) {
            $lastId = 0;
        }
        $kategori = new Kategori;
        $kategori->nama = $request->nama;
        $kategori->kode = 'KTG-' . str_pad($lastId->id + 1, 3, 0, STR_PAD_LEFT);
        $kategori->jenis_surat =  $request->jenis;
        $kategori->save();
        return redirect()->route('kategori.index')->with('success', 'Data kategori berhasil ditambahkan');
    }

    public function updateKategori($id)
    {
        $data = Kategori::find($id);
        return view('kategori_admin.edit-kategori', compact('data'));
    }

    public function editKategori(Request $request, $id)
    {
        $data = Kategori::find($id);
        $data->nama = $request->nama;
        $data->jenis_surat =  $request->jenis;
        $data->save();
        return redirect()->route('kategori.index')->with('success', 'Data kategori berhasil diupdate');
    }

    public function deleteKategori($id)
    {
        $data = Kategori::find($id);
        $data->status_active = false;
        $data->save();
        return redirect()->route('kategori.index')->with('error', 'Data kategori berhasil dihapus');
    }

    public function kategoriByJenis(Request $request)
    {
        $kategori = Kategori::where('jenis_surat', $request->jenis)->where('status_active', true)->get();
        return view('kategori_admin.daftar-kategori', compact('kategori'));
    }
}
