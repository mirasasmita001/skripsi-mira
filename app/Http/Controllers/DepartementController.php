<?php

namespace App\Http\Controllers;

use App\Models\Departement;
use Illuminate\Http\Request;

class DepartementController extends Controller
{


    public function index(Request $request)
    {
        $dept = Departement::where('status_active', true)->get();
        return view('departement_admin.daftar-departement', compact('dept'));
    }

    public function createDept()
    {
        return view('departement_admin.input-departement');
    }

    public function storeDept(Request $request)
    {
        $dept = new Departement;
        $dept->nama =  $request->nama;
        $dept->save();
        return redirect()->route('dept.index')->with('success', 'Data departement berhasil ditambahkan');
    }

    public function updateDept($id)
    {
        $data = Departement::find($id);
        return view('departement_admin.edit-departement', compact('data'));
    }

    public function editDept(Request $request, $id)
    {
        $data = Departement::find($id);
        $data->nama = $request->nama;
        $data->save();
        return redirect()->route('dept.index')->with('success', 'Data departement berhasil diupdate');
    }

    public function deleteDept($id)
    {
        $data = Departement::find($id);
        $data->status_active = false;
        $data->save();
        return redirect()->route('dept.index')->with('error', 'Data departement berhasil dihapus');
    }
}
